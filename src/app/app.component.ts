import { Component } from '@angular/core';

@Component({
  selector: 'fax-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'fedex';
}
