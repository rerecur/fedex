import { Injectable } from '@angular/core';
import { Option, some, none } from 'fp-ts/Option';

import { FeatureSet } from '../feature-set.enum';
import { Result } from '../result.enum';

/*
 * The PurchaseService is a stateful service that allows
 * the user to buy any specified FeatureSet, and to cancel
 * such a purchase.
 */
@Injectable({
  providedIn: 'root'
})
export class PurchaseService {

  /* Remember what the customer buys. */
  private _selection: Option<FeatureSet>;

  constructor() {
    this._selection = none;
  }

  /*
   * Return the selected FeatureSet, or none when
   * no purchase has been selected yet.
   */
  getFeatureSet(): Option<FeatureSet> {
    return this._selection;
  }

  /*
   * Buy the selected FeatureSet.
   */
  buy(featureSet: FeatureSet): Result {
    this._selection = some(featureSet);
    return Result.SUCCESS;
  }

  /*
   * Cancel any selected purchase.
   */
  cancel(): Result {
    this._selection = none;
    return Result.SUCCESS;
  }
}
