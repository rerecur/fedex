import { TestBed } from '@angular/core/testing';
import { Option, some, none, toUndefined } from 'fp-ts/Option';

import { FeatureSet } from '../feature-set.enum';
import { Result } from '../result.enum';
import { PurchaseService } from './purchase.service';

describe('PurchaseService', () => {
  let service: PurchaseService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(PurchaseService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should have no purchase when created', () => {
    expect(service.getFeatureSet()).toEqual(none);
  });

  it('should remember the purchase', () => {
    expect(service.buy(FeatureSet.ULTIMATE)).toEqual(Result.SUCCESS);
    expect(toUndefined(service.getFeatureSet())).toEqual(FeatureSet.ULTIMATE);
  });

  it('should cancel', () => {
    expect(service.buy(FeatureSet.REGULAR)).toEqual(Result.SUCCESS);
    expect(service.cancel()).toEqual(Result.SUCCESS);
    expect(service.getFeatureSet()).toEqual(none);
  });
});
