/*
 * The FeatureSet enum defines the various options that
 * the user can purchase. Presumably, each has its own
 * price, etc. And the user can select which choice is
 * desired.
 */
export enum FeatureSet {
  STARTER,
  REGULAR,
  PROFESSIONAL,
  ULTIMATE
}
