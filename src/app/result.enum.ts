/*
 * The Result enum acts as a well-named boolean, which
 * indicates the success or failure of certain function
 * calls, especially a service's methods.
 */
export enum Result {
  SUCCESS,
  FAILURE
}
